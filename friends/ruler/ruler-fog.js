document.write( '<style type="text/css">' + 
		'#donation-banner { background: #4cae4c; color: #fff; padding: 0 10px; font-size: 12px; }' +
		'#donation-banner a { color: #fff; }' +
		'#donation-banner p { display: inline-block; }' +
		'#donation-banner img { margin-right: 5px; width: 24px; height: 24px; vertical-align: middle; }' +
		'#donation-banner .donation-ruler { background: white; width: 210px; display: inline-block; padding: 2px; margin: 0 10px; }' +
		'#donation-banner .ninesix { margin: auto; width: 100%; text-align: center;}' +
		'#donation-banner .donation-button { border: 1px solid #555753; background-image: linear-gradient(to bottom, #73d216 0%, #4cae4c 100%); padding: 5px 10px; border-radius: 5px; text-decoration: none; color: #fff !important; font-weight: bold; }' +
		'</style>' )

document.write( '<div id="donation-banner">' +
			'<div class="ninesix">' +
			'<img src="https://static.gnome.org/friends/ruler/lock.png">' +
			'<p>Donate now to help GNOME <a href="https://us.commitchange.com/ca/orinda/gnome-foundation" style="margin-right: 20px;">continue our work!</a></p>' +
			'<a href="https://us.commitchange.com/ca/orinda/gnome-foundation" class="donation-button">Donate now!</a>' +
			'</div>' +
		'</div>' +

		'<div style="clear: both;"></div>')
